*** Settings ***
Library  BuiltIn

*** Variables ***
${area}=  0
${p}=  3.14
${r}=  4

*** Test Cases ***
1. คำนวณพื้นที่วงกลม
    ${area}=  CircleArea  ${r}
    Set Global Variable  ${area}

2. แสดงผล
    LOG to Console  \nArea: ${area}

*** Keywords ***
CircleArea
    [Arguments]  ${r}
    ${area}=  Evaluate  ${r} * ${r} * ${p}
    [Return]  ${area}
